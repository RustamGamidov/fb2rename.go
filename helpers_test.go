package main

import (
    "encoding/xml"
    "reflect"
    "testing"
)

// ref: https://github.com/benbjohnson/testing

// testAssert fails the test if the condition is false.
func testAssert(tb testing.TB, condition bool, msg string, v ...interface{}) {
    tb.Helper()
    if !condition {
        tb.Errorf("\033[31m"+msg+"\033[39m\n", v...)
    }
}

// testOK fails the test if an err is not nil.
func testOK(tb testing.TB, err error) {
    tb.Helper()
    if err != nil {
        tb.Errorf("\033[31m\nunexpected error: %#v\033[39m\n", err)
    }
}

// testNotOK fails the test if an err is nil.
func testNotOK(tb testing.TB, err error) {
    tb.Helper()
    if err == nil {
        tb.Errorf("\033[31m\nun error expected: %#v\033[39m\n", err)
    }
}

// testEquals fails the test if expected is not equal to actual.
func testEquals(tb testing.TB, expected, actual interface{}) {
    tb.Helper()
    if !reflect.DeepEqual(expected, actual) {
        tb.Errorf("\033[31m\nexp: %#v\nact: %#v\033[39m\n", expected, actual)
    }
}

func harmonizeAuthor(author authorType) authorType {
    author.XMLName = xml.Name{Local: "author"}
    return author
}

func mockValidSample() string {
    return `<FictionBook><description>
    <title-info>
      <genre>sci_medicine</genre><genre>science</genre>
      <author><first-name>Boris</first-name><last-name>Noname</last-name></author>
      <author><first-name>Boris2</first-name><middle-name>Noname2</middle-name></author>
      <book-title>Yes. Tile as it is.</book-title>
      <annotation><p>Book annotation. HTML tags can be present.</p></annotation>
      <date value="1984-01-01">1984</date>
      <coverpage><image l:href="#cover.jpg"/></coverpage>
      <lang>ru</lang><src-lang>ru</src-lang>
      <sequence name="This book sequence name" number="2"/>
    </title-info>
    <document-info>
      <author><first-name>author first name</first-name><middle-name>author middle name</middle-name>
      <last-name>author last name</last-name><nickname>author nick name</nickname></author>
    </document-info>
    </description></FictionBook>`
}

func mockEmptyTitleInfoSample() string {
    return `<FictionBook><description><title-info></title-info></description></FictionBook>`
}

func mockInvalidAuthorSample() string {
    return `<FictionBook><description>
    <title-info>
      <genre>sci_medicine</genre><genre>science</genre>
      <author></author>
    </title-info>
    </description></FictionBook>`
}

func mockSequenceNameOnlySample() string {
    return `<FictionBook><description><title-info>
      <sequence name="This book sequence name"/>
    </title-info></description></FictionBook>`
}

func mockSequenceNumberOnlySample() string {
    return `<FictionBook><description><title-info>
      <sequence  number="13"/>
    </title-info></description></FictionBook>`
}

func mockInvalidGenresSample() string {
    return `<FictionBook><description>
    <title-info></title-info>
    </description></FictionBook>`
}

func mockDateCaptionSample() string {
    return `<FictionBook><description><title-info>
    <date value="1983-01-01">1985</date>
    </title-info></description></FictionBook>`
}

func mockDateCaptionWrongValueSample() string {
    return `<FictionBook><description><title-info>
    <date value="wrong">1986</date>
    </title-info></description></FictionBook>`
}

func mockDateNoCaptionSample() string {
    return `<FictionBook><description><title-info>
    <date value="1982-01-01"></date>
    </title-info></description></FictionBook>`
}

func mockInvalidDateSample() string {
    return `<FictionBook><description><title-info>
    <date value="wrong"></date>
    </title-info></description></FictionBook>`
}
