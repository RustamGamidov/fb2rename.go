package main

import (
    "bytes"
    "encoding/xml"
    "fmt"
    "log"
    "regexp"
    "strings"
    "time"
)

type authorType struct {
    XMLName   xml.Name `xml:"author"`
    Firstname string   `xml:"first-name"`
    Midmane   string   `xml:"middle-name"`
    Lastname  string   `xml:"last-name"`
    Nickname  string   `xml:"nickname"`
    Email     string   `xml:"email"`
}

type sequenceType struct {
    XMLName xml.Name `xml:"sequence"`
    Name    string   `xml:"name,attr"`
    Number  int      `xml:"number,attr"`
}

func (obj authorType) formatName(format string) (string, error) {
    if obj.Firstname == "" && obj.Midmane == "" && obj.Lastname == "" {
        return "", fmt.Errorf("authorType: no data to format")
    }
    result := format
    result = strings.ReplaceAll(result, "#F", obj.Firstname)
    result = strings.ReplaceAll(result, "#L", obj.Lastname)
    result = strings.ReplaceAll(result, "#M", obj.Midmane)
    return strings.TrimSpace(result), nil
}

type fb2Type struct {
    XMLName     xml.Name `xml:"fictionbook"`
    Description struct {
        XMLName   xml.Name `xml:"description"`
        TitleInfo struct {
            XMLName  xml.Name     `xml:"title-info"`
            Title    string       `xml:"book-title"`
            Authors  []authorType `xml:"author"`
            Genres   []string     `xml:"genre"`
            Sequence sequenceType `xml:"sequence"`
            Date     struct {
                XMLName xml.Name `xml:"date"`
                Caption string   `xml:",chardata"`
                Value   string   `xml:"value,attr"`
            }
        }
        DocumentInfo struct {
            XMLName xml.Name     `xml:"document-info"`
            Authors []authorType `xml:"author"`
        }
    }
}

func (obj fb2Type) title() string {
    return obj.Description.TitleInfo.Title
}

func (obj fb2Type) sequenceName() string {
    return obj.Description.TitleInfo.Sequence.Name
}

func (obj fb2Type) sequenceNumber() string {
    number := obj.Description.TitleInfo.Sequence.Number
    if number == 0 {
        return ""
    }
    return fmt.Sprint(number)
}

func (obj fb2Type) sequence() string {
    name := obj.sequenceName()
    number := obj.sequenceNumber()
    if number == "" {
        return name
    }
    if name == "" {
        name = "[unknown sequence]"
    }
    return fmt.Sprintf("%s-%s", name, number)
}

func (obj fb2Type) authors(format string) string {
    authors := []string{}
    for _, person := range obj.Description.TitleInfo.Authors {
        aname, err := person.formatName(format)
        if err != nil {
            log.Println("error: failed to parse author")
            continue
        }
        authors = append(authors, aname)
    }
    return strings.Join(authors, "; ")
}

func (obj fb2Type) genres() string {
    return strings.Join(obj.Description.TitleInfo.Genres, "; ")
}

func (obj fb2Type) date() string {
    caption := obj.Description.TitleInfo.Date.Caption
    if caption != "" {
        return caption
    }
    value, err := time.Parse("2006-01-02", obj.Description.TitleInfo.Date.Value)
    if err != nil {
        return caption
    }
    return fmt.Sprint(value.Year())
}

type tPlaceholders map[string]func() string

func (obj fb2Type) replacePlaceholders(tmplt string, placeholders tPlaceholders) (string, error) {
    result := tmplt

    for placeholder, getter := range placeholders {
        result = strings.ReplaceAll(result, placeholder, getter())
    }
    return result, nil
}

func tagsToLower(in string) []byte {
    re := regexp.MustCompile(`<.*?\w+`)
    out := re.ReplaceAllFunc([]byte(in), bytes.ToLower)
    return out
}

func createFB2(content string) (*fb2Type, error) {
    var fb2 fb2Type
    xml.Unmarshal(tagsToLower(content), &fb2)
    return &fb2, nil
}

func main() {

}
