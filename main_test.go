package main

import (
    "testing"
)

// -----------------------------------------------------------------------------
// -- createFB2 ----------------------------------------------------------------

func Test_createFB2_returnEmptyAuthors_whenGetEmptyString(t *testing.T) {
    obj, _ := createFB2("")
    authors := obj.Description.DocumentInfo.Authors
    testEquals(t, 0, len(authors))
}

func Test_createFB2_returnExpectedDocAuthors_whenGetValidInputAndSingleAuthor(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    authors := obj.Description.DocumentInfo.Authors
    testEquals(t, 1, len(authors))
    testEquals(t, harmonizeAuthor(authorType{
        Firstname: "author first name", Midmane: "author middle name",
        Lastname: "author last name", Nickname: "author nick name",
    }), authors[0])
}

func Test_createFB2_returnExpectedTitleAuthors_whenGetValidInputAndSeveralAuthors(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    authors := obj.Description.TitleInfo.Authors
    testEquals(t, 2, len(authors))
    testEquals(t, harmonizeAuthor(authorType{Firstname: "Boris", Lastname: "Noname"}), authors[0])
    testEquals(t, harmonizeAuthor(authorType{Firstname: "Boris2", Midmane: "Noname2"}), authors[1])
}

func Test_createFB2_returnExpectedSequence_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "This book sequence name", obj.Description.TitleInfo.Sequence.Name)
    testEquals(t, 2, obj.Description.TitleInfo.Sequence.Number)
}

// -----------------------------------------------------------------------------
// -- formatName ---------------------------------------------------------------

func Test_formatName_returnErrorWhenEmptySource(t *testing.T) {
    _, err := authorType{}.formatName("format")
    testNotOK(t, err)
}

func Test_formatName_returnNoErrorWhenSourceNotEmpty(t *testing.T) {
    _, err := authorType{Firstname: "value"}.formatName("format")
    testOK(t, err)
}

func Test_formatName_returnExpectedValueWhenInputIsValid(t *testing.T) {
    muteErr := func(a authorType, f string) string { v, err := a.formatName(f); testOK(t, err); return v }

    a := authorType{Firstname: "fname", Lastname: "lname", Midmane: "mname"}
    testEquals(t, "fname mname lname", muteErr(a, "#F #M #L"))
    testEquals(t, "lname, fname mname", muteErr(a, "#L, #F #M"))
    testEquals(t, "fname lname", muteErr(a, "#F #L"))
    testEquals(t, "fnamelname", muteErr(a, "#F#L"))
    testEquals(t, "fname#lname", muteErr(a, "#F##L"))
}

// -----------------------------------------------------------------------------
// -- sequence -----------------------------------------------------------------

func Test_sequence_returnExpectedValue_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "This book sequence name-2", obj.sequence())
}

func Test_sequence_returnExpectedValue_whenGetSequenceNameOnlyPresent(t *testing.T) {
    obj, _ := createFB2(mockSequenceNameOnlySample())
    testEquals(t, "This book sequence name", obj.sequence())
}

func Test_sequence_returnExpectedValue_whenGetSequenceNumberOnlyPresent(t *testing.T) {
    obj, _ := createFB2(mockSequenceNumberOnlySample())
    testEquals(t, "[unknown sequence]-13", obj.sequence())
}

// -----------------------------------------------------------------------------
// -- authors ------------------------------------------------------------------

func Test_authors_returnExpectedValue_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "Noname, Boris; , Boris2 Noname2", obj.authors("#L, #F #M"))
}

func Test_authors_returnEmptyString_whenGetInvalidInput(t *testing.T) {
    obj, _ := createFB2(mockInvalidAuthorSample())
    testEquals(t, "", obj.authors("#L, #F #M"))
}

// -----------------------------------------------------------------------------
// -- genres -------------------------------------------------------------------

func Test_genres_returnExpectedValue_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "sci_medicine; science", obj.genres())
}

func Test_genres_returnEmptyString_whenGetInvalidInput(t *testing.T) {
    obj, _ := createFB2(mockInvalidGenresSample())
    testEquals(t, "", obj.genres())
}

// -----------------------------------------------------------------------------
// -- title --------------------------------------------------------------------

func Test_title_returnExpectedValue_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "Yes. Tile as it is.", obj.title())
}

func Test_title_returnEmptyString_whenNoTitleInTheInput(t *testing.T) {
    obj, _ := createFB2(mockEmptyTitleInfoSample())
    testEquals(t, "", obj.title())
}

// -----------------------------------------------------------------------------
// -- date ---------------------------------------------------------------------

func Test_date_returnExpectedValue_whenGetValidInput(t *testing.T) {
    obj, _ := createFB2(mockValidSample())
    testEquals(t, "1984", obj.date())
}

func Test_date_returnCaptionString_whenPresent(t *testing.T) {
    obj, _ := createFB2(mockDateCaptionSample())
    testEquals(t, "1985", obj.date())
}

func Test_date_returnValueString_whenNoCaption(t *testing.T) {
    obj, _ := createFB2(mockDateNoCaptionSample())
    testEquals(t, "1982", obj.date())
}

func Test_date_returnValueString_whenGetInvalidInput(t *testing.T) {
    obj, _ := createFB2(mockInvalidDateSample())
    testEquals(t, "", obj.date())
}

// -----------------------------------------------------------------------------
// -- replacePlaceholders-------------------------------------------------------

func Test_replacePlaceholders_returnsInputString_whenListOfPlaceholdersIsEmpty(t *testing.T) {
    obj, _ := createFB2(mockValidSample())

    template := `%seq_name%\%seq_number%. %title%`
    result, err := obj.replacePlaceholders(template, tPlaceholders{})
    testOK(t, err)
    testEquals(t, template, result)
}

func Test_replacePlaceholders_returnsExpectedString_whenListOfPlaceholdersIsNotEmpty(t *testing.T) {
    obj, _ := createFB2(mockValidSample())

    placeholders := tPlaceholders{
        `%title%`:      func() string { return obj.title() },
        `%authors%`:    func() string { return obj.authors("#L, #F #M") },
        `%genres%`:     func() string { return obj.genres() },
        `%sequence%`:   func() string { return obj.sequence() },
        `%seq_name%`:   func() string { return obj.sequenceName() },
        `%seq_number%`: func() string { return obj.sequenceNumber() },
        `%series%`:     func() string { return obj.sequence() },
    }

    ref := "sci_medicine; science\\[Noname, Boris; , Boris2 Noname2] plain Yes. Tile as it is. {2} text"
    result, err := obj.replacePlaceholders(`%genres%\[%authors%] plain %title% {%seq_number%} text`, placeholders)
    testOK(t, err)
    testEquals(t, ref, result)
}
